import {NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {AuthService} from "app/shared/auth.service";
import {LoginUserComponent} from "app/sections/login-user/login-user.component";
import {DisplayUserComponent} from "app/sections/display-user/display-user.component";
import {RegisterUserComponent} from "app/register-user/register-user.component";
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import {PlaylistComponent} from "./sections/playlist/playlist.component";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule, PreloadAllModules} from "@angular/router";
import {routes} from "./app.routing";
import {StoreModule} from "@ngrx/store";
import {reducer} from "./reducer";
import {RouterStoreModule} from "@ngrx/router-store";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {AlertModule} from "ng2-bootstrap/index";
import {AngularFireModule} from "angularfire2/index";
import {firebaseConfig, authConfig} from "../environments/firebaseConfig";
import {LoginComponent} from "./sections/login.component";
import {PageNotFoundComponent} from "./not-found.component";

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        PageNotFoundComponent,
        DisplayUserComponent,
        LoginUserComponent,
        RegisterUserComponent,
        ResetPasswordComponent,
        PlaylistComponent
    ],
    imports: [
      BrowserModule,
      HttpModule,
      RouterModule.forRoot(routes, {useHash: true, preloadingStrategy: PreloadAllModules}),
      StoreModule.provideStore(reducer),
      RouterStoreModule.connectRouter(),
      // Redux dev tools integration import must come *after* StoreModule and RouterStoreModule.
      StoreDevtoolsModule.instrumentOnlyWithExtension(),
      ReactiveFormsModule,
      FormsModule,
      AlertModule.forRoot(),
      AngularFireModule.initializeApp(firebaseConfig, authConfig)
    ],
    providers: [AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
