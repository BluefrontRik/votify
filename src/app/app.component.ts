import { Component, ChangeDetectionStrategy, OnInit, ElementRef, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from './reducer';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app-root',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="app">
        <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {
  constructor(private elm: ElementRef,
              private store: Store<RootState>) {
  }
}
