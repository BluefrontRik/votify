import { Routes } from '@angular/router';
import {PlaylistComponent} from "./sections/playlist/playlist.component";
import {AppComponent} from "./app.component";
import {PageNotFoundComponent} from "./not-found.component";
import {LoginComponent} from "./sections/login.component";

export const routes: Routes = [
  { path: 'playlist', component: PlaylistComponent },
  { path: 'login', component: LoginComponent },
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];
