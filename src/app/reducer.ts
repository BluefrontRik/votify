import '@ngrx/core/add/operator/select';
import { compose } from '@ngrx/core';
import { combineReducers, ActionReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import * as fromRouter from '@ngrx/router-store';

// Import all reducers and selectors.

// Define the global app state.
export interface RootState {
  router: fromRouter.RouterState;
}

const reducers = Object.assign({
  router: fromRouter.routerReducer

});

// Generate a reducer to set the root state, used for HMR.
function stateSetter(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    if (action.type === 'SET_ROOT_STATE') {
      return action.payload;
    }
    return reducer(state, action);
  };
}

const developmentReducer = <Function>compose(stateSetter, storeFreeze, combineReducers)(reducers);
// const productionReducer = combineReducers(reducers);

export function reducer(state: any, action: any) {
    return developmentReducer(state, action);
}
