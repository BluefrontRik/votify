import { Component, ChangeDetectionStrategy, OnInit, OnDestroy, ViewEncapsulation, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootState } from '../../reducer';


@Component({
  // changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'playlist.component.html',
  styleUrls: ['playlist.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class PlaylistComponent implements OnInit, OnDestroy {
  public test = '';

  constructor(private store: Store<RootState>) {
  }

  ngOnInit() {
    this.test = 'Select your playlist';

  }

  ngOnDestroy() {
  }

}
