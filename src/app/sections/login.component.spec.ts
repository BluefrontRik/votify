/* tslint:disable:no-unused-variable */
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {TestBed, async} from "@angular/core/testing";
import {LoginComponent} from "./login.component";
import {AuthService} from "app/shared/auth.service";
import {AuthServiceStub} from "app/shared/auth.service.stub";
import {AlertModule} from "ng2-bootstrap";

describe('LoginComponent', () => {
    beforeEach(() => {
        let authServiceStub = new AuthServiceStub(true);

        TestBed.configureTestingModule({
            declarations: [
                LoginComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [
                AlertModule.forRoot()
            ],
            providers: [
                {provide: AuthService, useValue: authServiceStub}
            ]
        });;
    });

    it('should create the app', async(() => {
        let fixture = TestBed.createComponent(LoginComponent);
        let app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it(`should have as title 'app works!'`, async(() => {
        let fixture = TestBed.createComponent(LoginComponent);
        let app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('app works! - votetify');
    }));

    it('should render title in a h1 tag', async(() => {
        let fixture = TestBed.createComponent(LoginComponent);
        fixture.detectChanges();
        let compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('app works! - votetify');
    }));
});
