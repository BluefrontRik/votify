
import {AuthMethods, AuthProviders} from "angularfire2";


export const firebaseConfig = {
    // Paste all this from the Firebase console...
    apiKey: "AIzaSyAZFg4lHNNZBL08T_fys2oLtvX2M-nZonw",
    authDomain: "votify-927c3.firebaseapp.com",
    databaseURL: "https://votify-927c3.firebaseio.com",
    storageBucket: "votify-927c3.appspot.com",
    messagingSenderId: "271998482709"
};

export const authConfig = {
    provider: AuthProviders.Password,
    method: AuthMethods.Password
};
